package com.jj.cephalonvigil.api.parser

import com.google.gson.Gson
import com.google.gson.JsonDeserializationContext
import com.google.gson.JsonDeserializer
import com.google.gson.JsonElement
import com.google.gson.JsonParseException
import com.jj.cephalonvigil.model.MissionReward

import java.lang.reflect.Type

/**
 * Created by D053411 on 11/12/2016.
 */
class MissionRewardDeserializer implements JsonDeserializer<MissionReward> {

    private static final Gson DELEGATE = new Gson()

    @Override
    MissionReward deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
        if(json.isJsonArray()) return null
        DELEGATE.fromJson json, typeOfT
    }
}
