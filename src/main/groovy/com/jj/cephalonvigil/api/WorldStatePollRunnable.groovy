package com.jj.cephalonvigil.api

import com.google.gson.Gson
import com.google.gson.GsonBuilder
import com.jj.cephalonvigil.api.parser.MissionRewardDeserializer
import com.jj.cephalonvigil.model.MissionReward
import com.jj.cephalonvigil.model.WorldState
import io.restassured.RestAssured
import io.restassured.config.DecoderConfig
import io.restassured.config.EncoderConfig
import io.restassured.config.ObjectMapperConfig
import io.restassured.config.RestAssuredConfig
import io.restassured.mapper.ObjectMapperType
import io.restassured.mapper.factory.GsonObjectMapperFactory

/**
 * Created by D053411 on 11/6/2016.
 */
class WorldStatePollRunnable implements Runnable {

    private static final String WORLD_STATE_URL = "http://content.warframe.com/dynamic/worldState.php"

    rx.Subscriber<WorldState> subscriber

    static {
        RestAssured.config = new RestAssuredConfig().
                decoderConfig(
                        new DecoderConfig("UTF-8")
                ).encoderConfig(
                new EncoderConfig("UTF-8", "UTF-8")
        ).objectMapperConfig(
                new ObjectMapperConfig(ObjectMapperType.GSON).
                        gsonObjectMapperFactory(new GsonObjectMapperFactory() {
                            public Gson create(Class cls, String charset) {
                                new GsonBuilder().registerTypeAdapter(MissionReward, new MissionRewardDeserializer()).create()
                            }
                        })
        )
    }

    @Override
    void run() {
        try {
            println "Polling world state..."
            subscriber.onNext RestAssured.get(WORLD_STATE_URL).body.as(WorldState, ObjectMapperType.GSON)
        } catch(e) {
            e.printStackTrace()
            throw new RuntimeException(e)
        }
    }

}
