package com.jj.cephalonvigil.api

import com.jj.cephalonvigil.model.WorldState

import java.util.concurrent.Executors
import java.util.concurrent.ScheduledExecutorService
import java.util.concurrent.TimeUnit

/**
 * Created by D053411 on 11/6/2016.
 */
class CurrentWorldState {

    static final ScheduledExecutorService EXECUTOR = Executors.newScheduledThreadPool 5
    static final int POLLING_INTERVAL = 60

    static rx.Observable<WorldState> observe() {
        println "Starting scheduler..."
        rx.Observable.create { s ->
            EXECUTOR.scheduleAtFixedRate(new WorldStatePollRunnable(subscriber: s), 0, POLLING_INTERVAL, TimeUnit.SECONDS)
            null
        }
    }

}
