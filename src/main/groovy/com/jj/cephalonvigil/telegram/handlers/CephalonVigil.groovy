package com.jj.cephalonvigil.telegram.handlers

import com.jj.cephalonvigil.api.CurrentWorldState
import com.jj.cephalonvigil.db.Storage
import com.jj.cephalonvigil.model.*
import com.jj.cephalonvigil.telegram.commands.FilterCommand
import com.jj.cephalonvigil.telegram.commands.HelpCommand
import com.jj.cephalonvigil.telegram.commands.HowToCommand
import com.jj.cephalonvigil.telegram.commands.PlatformCommand
import com.jj.cephalonvigil.telegram.commands.StartCommand
import com.jj.cephalonvigil.telegram.commands.TimeZoneCommand
import org.telegram.telegrambots.api.methods.send.SendMessage
import org.telegram.telegrambots.api.objects.Update
import org.telegram.telegrambots.bots.TelegramLongPollingCommandBot

import java.text.SimpleDateFormat

/**
 * Created by Michael on 8/11/2016.
 */
class CephalonVigil extends  TelegramLongPollingCommandBot  {
    private static final String BOT_KEY = System.getProperty("VIGIL_BOT_KEY") ?: System.getenv()["VIGIL_BOT_KEY"]
    private static final String BOT_USERNAME = "Cephalon Vigil"
    private static final Filter UNKNOWN_ITEM_FILTER = new UnknownItemFilter()

    private final Storage storage

    CephalonVigil(Storage storage) {
        this.storage = storage
        register new HelpCommand(this)
        register new HowToCommand()
        register new StartCommand(storage: storage)
        register new FilterCommand(storage: storage)
        register new TimeZoneCommand(storage: storage)
        register new PlatformCommand(storage: storage)

        observeWorldState()
    }

    @Override
    void processNonCommandUpdate(Update update) {
    }

    @Override
    String getBotUsername() {
        return BOT_USERNAME
    }

    @Override
    String getBotToken() {
        return BOT_KEY
    }

    void observeWorldState() {
        CurrentWorldState.observe()
        .forEach { WorldState worldState ->
            try {
                println "Processing world state..."
                storage.regexFilters.each { regexFilter ->
                    sendFilteredItemNotifications worldState, regexFilter
                }
                sendUnknownItemNotifications worldState
                sendAcolyteNotifications worldState.PersistentEnemies
            } catch(e) {
                e.printStackTrace()
            }
        }
    }

    /**
     * Send a notification to each subscriber for each matching item.
     *
     * @param worldState
     * @param filter
     */
    void sendFilteredItemNotifications(WorldState worldState, Filter filter) {
        sendAlertNotifications worldState.Alerts, filter
        sendInvasionNotifications worldState.Invasions, filter
    }

    /**
     * Send a notification to the bot owner for each unknown item.
     *
     * @param worldState
     */
    void sendUnknownItemNotifications(WorldState worldState) {
        sendAlertNotifications worldState.Alerts, UNKNOWN_ITEM_FILTER
        sendInvasionNotifications worldState.Invasions, UNKNOWN_ITEM_FILTER
    }

    void sendAlertNotifications(List<Alert> alerts, Filter filter) {
        filter.alerts(alerts).findAll {
            this.filterSentNotifications(filter.chatId, it.id)
        }.each {
            sendAlertNotification filter.chatId, it
        }
    }

    void sendAcolyteNotifications(List<PersistentEnemy> acolytes) {
        storage.getChatIds().forEach { chatId ->
            acolytes.findAll {
                this.filterSentNotifications(chatId, it.eventId)
            }.each {
                sendAcolyteNotification chatId, it
            }
        }
    }

    void sendAlertNotification(String chatId, Alert alert) {
        SimpleDateFormat dateFormat = new SimpleDateFormat("HH:mm:ss z")
        dateFormat.setTimeZone storage.getTimeZone(chatId)
        String expiry = dateFormat.format(alert.Expiry as Date)

        SendMessage alertMessage = new SendMessage();
        alertMessage.setChatId chatId
        alertMessage.enableMarkdown(true)
        alertMessage.setText alert.MissionInfo.prettyString + ", till $expiry"
        sendMessage alertMessage

        storeSentNotification chatId, alert.id
    }

    void sendInvasionNotifications(List<Invasion> invasions, Filter filter) {
        filter.invasions(invasions).findAll {
            filterSentNotifications filter.chatId, it.id
        }.each {
            sendInvasionNotification filter.chatId, it
        }
    }

    void sendInvasionNotification(String chatId, Invasion invasion) {
        SendMessage invasionMessage = new SendMessage();
        invasionMessage.setChatId chatId
        invasionMessage.enableMarkdown(true)
        invasionMessage.setText invasion.prettyString
        sendMessage invasionMessage

        storeSentNotification chatId, invasion.id
    }

    void sendAcolyteNotification(String chatId, PersistentEnemy acolyte) {
        SendMessage acolyteMessage = new SendMessage();
        acolyteMessage.setChatId chatId
        acolyteMessage.enableMarkdown(true)
        if(acolyte.Discovered) {
            acolyteMessage.setText "$acolyte.prettyAgentName has been discovered at $acolyte.prettyLocation."
        } else if (acolyte.LastDiscoveredLocation) {
            acolyteMessage.setText "$acolyte.prettyAgentName has moved on."
        } else {
            acolyteMessage.setText "$acolyte.prettyAgentName has returned."
        }
        sendMessage acolyteMessage

        storeSentNotification chatId, acolyte.eventId
    }

    boolean filterSentNotifications(String chatId, String id) {
        !storage.wasNotified(chatId, id)
    }

    void storeSentNotification(String chatId, String id) {
        storage.notify chatId, id
    }
}
