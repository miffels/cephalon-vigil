package com.jj.cephalonvigil.telegram.commands

import com.jj.cephalonvigil.db.Storage
import com.jj.cephalonvigil.model.Platform
import org.telegram.telegrambots.api.methods.send.SendMessage
import org.telegram.telegrambots.api.objects.Chat
import org.telegram.telegrambots.api.objects.User
import org.telegram.telegrambots.bots.AbsSender

/**
 * Created by Michael on 8/11/2016.
 */
class PlatformCommand extends BaseCommand {

    Storage storage

    PlatformCommand() {
        super("platform", "Set the platform for the current chat");
    }

    @Override
    void execute(AbsSender sender, User user, Chat chat, String[] arguments) {
        if(arguments.length > 1) { sendTooManyArguments sender, chat; return }

        if(!arguments.length) {
            sendPlatformMessage storage.getPlatform("$chat.id"), sender, chat
            return
        }

        Platform platform = Platform.valueOf arguments[0]

        if(!platform) sendInvalidPlatformMessage arguments[0], sender, chat
        else {
            storage.savePlatform chat, platform

            sendSavedMessage sender, chat
        }
    }

    void sendInvalidPlatformMessage(String platform, AbsSender sender, Chat chat) {
        SendMessage message = new SendMessage()
        message.setChatId chat.id.toString()
        message.enableMarkdown true
        message.disableWebPagePreview()
        message.setText "I do not know '$platform', Operator. " +
                "Supported values: ${Platform.values().join(', ')}"

        send(message, sender)
    }

    void sendPlatformMessage(Platform platform, AbsSender sender, Chat chat) {
        SendMessage message = new SendMessage()
        message.setChatId chat.id.toString()
        message.setText "Your current platform is $platform."

        send(message, sender);
    }

    void sendSavedMessage(AbsSender sender, Chat chat) {
        SendMessage message = new SendMessage()
        message.setChatId chat.id.toString()
        message.setText "Platform updated."

        send(message, sender);
    }

}
