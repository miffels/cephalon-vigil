package com.jj.cephalonvigil.telegram.commands

import org.telegram.telegrambots.api.methods.send.SendMessage
import org.telegram.telegrambots.api.objects.Chat
import org.telegram.telegrambots.api.objects.User
import org.telegram.telegrambots.bots.AbsSender
import org.telegram.telegrambots.bots.commands.BotCommand
import org.telegram.telegrambots.bots.commands.ICommandRegistry

/**
 * Created by Michael on 8/11/2016.
 */
class HelpCommand extends BaseCommand {
    static def LOGTAG = "HelpCommand"

    def commandRegistry

    HelpCommand(ICommandRegistry commandRegistry) {
        super("help", "Get all the commands this bot provides")
        this.commandRegistry = commandRegistry
    }

    @Override
    void execute(AbsSender sender, User user, Chat chat, String[] arguments) {
        if(arguments.length) { sendTooManyArguments sender, chat; return }

        sendHelpMessage sender, chat
    }

    void sendHelpMessage(sender, chat) {
        StringBuilder helpMessageBuilder = new StringBuilder("<b>Help</b>\n");
        helpMessageBuilder.append "These are the registered commands for this Bot:\n\n"

        for (BotCommand botCommand : commandRegistry.getRegisteredCommands()) {
            helpMessageBuilder.append(botCommand.toString()).append "\n\n"
        }

        SendMessage helpMessage = new SendMessage();
        helpMessage.setChatId chat.id.toString()
        helpMessage.disableWebPagePreview()
        helpMessage.enableHtml true
        helpMessage.setText helpMessageBuilder.toString()

        send(helpMessage, sender)
    }

}
