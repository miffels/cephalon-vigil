package com.jj.cephalonvigil.telegram.commands

import com.jj.cephalonvigil.db.Storage
import org.telegram.telegrambots.api.methods.send.SendMessage
import org.telegram.telegrambots.api.objects.Chat
import org.telegram.telegrambots.api.objects.User
import org.telegram.telegrambots.bots.AbsSender

/**
 * Created by Michael on 8/11/2016.
 */
class TimeZoneCommand extends BaseCommand {

    Storage storage

    TimeZoneCommand() {
        super("timezone", "Set the time zone for the current chat");
    }

    @Override
    void execute(AbsSender sender, User user, Chat chat, String[] arguments) {
        if(arguments.length > 1) { sendTooManyArguments sender, chat; return }

        if(!arguments.length) {
            sendTimeZoneMessage storage.getTimeZone("$chat.id"), sender, chat
            return
        }

        TimeZone timeZone = TimeZone.getTimeZone(arguments[0], false)

        if(!timeZone) sendInvalidTimezoneMessage arguments[0], sender, chat
        else {
            storage.saveTimeZone chat, timeZone

            sendSavedMessage sender, chat
        }
    }

    void sendInvalidTimezoneMessage(String timeZoneId, AbsSender sender, Chat chat) {
        SendMessage message = new SendMessage()
        message.setChatId chat.id.toString()
        message.enableMarkdown true
        message.disableWebPagePreview()
        message.setText "I do not know '$timeZoneId', Operator. " +
                "See [this list of abbrevations](https://en.wikipedia.org/wiki/List_of_time_zone_abbreviations) " +
                "and /howto for examples."

        send(message, sender)
    }

    void sendTimeZoneMessage(TimeZone timeZone, AbsSender sender, Chat chat) {
        SendMessage message = new SendMessage()
        message.setChatId chat.id.toString()
        message.setText "Your current time zone is $timeZone.ID."

        send(message, sender);
    }

    void sendSavedMessage(AbsSender sender, Chat chat) {
        SendMessage message = new SendMessage()
        message.setChatId chat.id.toString()
        message.setText "Time zone updated."

        send(message, sender);
    }

}
