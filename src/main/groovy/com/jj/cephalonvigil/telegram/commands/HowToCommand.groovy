package com.jj.cephalonvigil.telegram.commands

import org.telegram.telegrambots.api.methods.send.SendMessage
import org.telegram.telegrambots.api.objects.Chat
import org.telegram.telegrambots.api.objects.User
import org.telegram.telegrambots.bots.AbsSender

/**
 * Created by Michael on 8/11/2016.
 */
class HowToCommand extends BaseCommand {

    HowToCommand() {
        super("howto", "Learn how to use this cephalon")
    }

    @Override
    void execute(AbsSender sender, User user, Chat chat, String[] arguments) {
        if(arguments.length) { sendTooManyArguments sender, chat; return }

        sendHelpMessage sender, chat
    }

    void sendHelpMessage(sender, chat) {
        String howToMessage = "Welcome back, Operator. I am Cephalon Vigil. My purpose is to keep you informed " +
                "about ongoing invasions and alerts with rewards matching your criteria. " +
                "Use [regular expressions](https://en.wikipedia.org/wiki/Regular_expression) to " +
                "tell me what to look for. See these examples:\n\n" +
                "/filter .\n" +
                "  Show all alerts and invasions that have item rewards\n" +
                "/filter Nitain\n" +
                "  Notifies you about alerts and invasions that give Nitain Extract\n" +
                "/filter Vauban\n" +
                "  Notifies you about alerts and invasions that give Vauban part blueprints\n" +
                "/filter Orokin|Alloy\n" +
                "  Notifies you about alerts and invasions that give Orokin Cells/Catalysts/Reactors/... or Alloy Plates \n" +
                "/filter\n" +
                "  Tells your currently configured filter\n\n" +

                "You can also tell me which time zone to use for alerts:\n" +
                "/timezone CET\n" +
                "  Format alert expiration dates in CET time zone\n" +
                "/timezone\n" +
                "  Tells your currently configured time zone\n\n" +

                "This is how to set your platform for alerts:\n" +
                "/platform PC\n" +
                "  Sets the platform for alerts to PC\n" +
                "/platform\n" +
                "  Tells your currently configured platform"

        SendMessage helpMessage = new SendMessage();
        helpMessage.setChatId chat.id.toString()
        helpMessage.disableWebPagePreview()
        helpMessage.enableMarkdown true
        helpMessage.setText howToMessage

        send(helpMessage, sender)
    }

}
