package com.jj.cephalonvigil.telegram.commands

import com.jj.cephalonvigil.db.Storage
import org.telegram.telegrambots.api.methods.send.SendMessage
import org.telegram.telegrambots.api.objects.Chat
import org.telegram.telegrambots.api.objects.User
import org.telegram.telegrambots.bots.AbsSender

/**
 * Created by Michael on 8/11/2016.
 */
class StartCommand extends BaseCommand {

    Storage storage

    StartCommand() {
        super("start", "Start interacting with this bot");
    }

    @Override
    void execute(AbsSender sender, User user, Chat chat, String[] arguments) {
        storage.saveChat chat

        sendWelcomeMessage sender, chat
    }

    void sendWelcomeMessage(AbsSender sender, Chat chat) {
        SendMessage message = new SendMessage()
        message.setChatId chat.id.toString()
        message.setText "Welcome back, Operator. It has been a long time, so you might want to take a look at /howto."

        send(message, sender);
    }

}
