package com.jj.cephalonvigil.telegram.commands

import com.jj.cephalonvigil.db.Storage
import com.jj.cephalonvigil.model.RegexFilter
import org.telegram.telegrambots.api.methods.send.SendMessage
import org.telegram.telegrambots.api.objects.Chat
import org.telegram.telegrambots.api.objects.User
import org.telegram.telegrambots.bots.AbsSender

import java.util.regex.Pattern

/**
 * Created by Michael on 8/11/2016.
 */
class FilterCommand extends BaseCommand {

    Storage storage

    FilterCommand() {
        super("filter", "Filter alerts matching the expressions");
    }

    @Override
    void execute(AbsSender sender, User user, Chat chat, String[] arguments) {
        if(!arguments.length) {
            sendFilterMessage storage.getRegexFilter(chat), sender, chat
            return
        }

        String patternWithSpaces = arguments.join(" ") // Merge regexes with spaces

        try {
            storage.saveFilter chat, Pattern.compile(patternWithSpaces)
            sendSavedMessage sender, chat
        } catch(e) {
            sendFailedMessage(patternWithSpaces, sender, chat)
        }
    }

    void sendSavedMessage(AbsSender sender, Chat chat) {
        SendMessage message = new SendMessage()
        message.setChatId chat.id.toString()
        message.setText "Filter updated."

        send(message, sender);
    }

    void sendFilterMessage(RegexFilter filter, AbsSender sender, Chat chat) {
        SendMessage message = new SendMessage()
        message.setChatId chat.id.toString()
        message.setText "Your current filter is '$filter.pattern'"

        send(message, sender);
    }

    void sendFailedMessage(String pattern, AbsSender sender, Chat chat) {
        SendMessage message = new SendMessage()
        message.setChatId chat.id.toString()
        message.setText "'$pattern' does not seem to make sense, Operator. Did you try /howto?"

        send(message, sender)
    }

}
