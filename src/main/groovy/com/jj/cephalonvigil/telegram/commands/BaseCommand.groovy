package com.jj.cephalonvigil.telegram.commands

import org.telegram.telegrambots.api.methods.send.SendMessage
import org.telegram.telegrambots.bots.commands.BotCommand
import org.telegram.telegrambots.exceptions.TelegramApiException
import org.telegram.telegrambots.logging.BotLogger

/**
 * Created by Michael on 8/11/2016.
 */
abstract class BaseCommand extends BotCommand {

    static def LOGTAG = "BaseCommand"

    BaseCommand(command, description) {
        super(command, description);
    }

    void sendTooManyArguments(sender, chat) {
        SendMessage message = new SendMessage();
        message.setChatId chat.id.toString()
        message.setText "Too much information, master! Usage: /register <token>"

        send message, sender
    }

    void sendFailedMessage(e = null, sender, chat) {
        SendMessage message = new SendMessage()
        message.setChatId chat.id.toString()
        def affix = e ? ": $e.message" : "."
        message.setText "I am afraid I could not read your data, master$affix " +
                "Please try again later."

        send(message, sender);
    }


    void send(message, sender) {
        try {
            sender.sendMessage message
        } catch (TelegramApiException e) {
            BotLogger.error LOGTAG, e
        }
    }

}
