package com.jj.cephalonvigil.telegram

import com.jj.cephalonvigil.db.Storage
import com.jj.cephalonvigil.telegram.handlers.CephalonVigil
import org.telegram.telegrambots.ApiContextInitializer
import org.telegram.telegrambots.TelegramBotsApi

/**
 * Created by Michael on 8/11/2016.
 */
class Bots {
    static def telegram = new TelegramBotsApi()

    static def register() {
        ApiContextInitializer.init()
        telegram.registerBot new CephalonVigil(new Storage())
    }

}
