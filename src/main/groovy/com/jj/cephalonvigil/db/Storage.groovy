package com.jj.cephalonvigil.db

import com.jj.cephalonvigil.model.Platform
import com.jj.cephalonvigil.model.RegexFilter
import groovy.sql.Sql
import org.telegram.telegrambots.api.objects.Chat

import java.util.regex.Pattern

/**
 * Created by Michael on 8/11/2016.
 */
class Storage {
    static final def DDL = '''
        CREATE TABLE IF NOT EXISTS ALERT_FILTERS(
            CHATID VARCHAR(255) PRIMARY KEY,
            EXPRESSION VARCHAR(255) DEFAULT '.',
            TIMEZONEID VARCHAR(255) DEFAULT 'UTC',
            PLATFORM VARCHAR(3) DEFAULT 'PC');

        CREATE TABLE IF NOT EXISTS SENT_NOTIFICATIONS(
            CHATID VARCHAR(255),
            ALERTID VARCHAR(255),
            PRIMARY KEY(CHATID, ALERTID));
    '''

    static final def SAVE_CHAT = 'MERGE INTO ALERT_FILTERS (CHATID) VALUES (?)'

    static final def SAVE_FILTER = 'MERGE INTO ALERT_FILTERS (CHATID, EXPRESSION) VALUES (?, ?)'

    static final def GET_FILTER = 'SELECT * FROM ALERT_FILTERS WHERE CHATID = ?'
    static final def GET_FILTERS = 'SELECT * FROM ALERT_FILTERS WHERE PLATFORM = ?'

    static final def GET_CHATS = 'SELECT * FROM ALERT_FILTERS WHERE PLATFORM = ?'

    static final def INSERT_NOTIFICATION = 'INSERT INTO SENT_NOTIFICATIONS (CHATID, ALERTID) VALUES (?, ?)'
    static final def FIND_NOTIFICATION = 'SELECT * FROM SENT_NOTIFICATIONS WHERE CHATID = ? AND ALERTID = ?'

    static final def SAVE_TIMEZONE = 'MERGE INTO ALERT_FILTERS (CHATID, TIMEZONEID) VALUES (?, ?)'
    static final def FIND_TIMEZONE = 'SELECT * FROM ALERT_FILTERS WHERE CHATID = ?'

    static final def SAVE_PLATFORM = 'MERGE INTO ALERT_FILTERS (CHATID, PLATFORM) VALUES (?, ?)'
    static final def FIND_PLATFORM = 'SELECT * FROM ALERT_FILTERS WHERE CHATID = ?'

    def sql = Sql.newInstance('jdbc:h2:./cephalon_vigil;CACHE_SIZE=5120', 'sa', 'sa', 'org.h2.Driver')

    Storage() {
        sql.execute(DDL)
    }

    RegexFilter getRegexFilter(Chat chat) {
        def row = sql.firstRow GET_FILTER, [chat.id]
        new RegexFilter(chatId: row.CHATID, expression: row.EXPRESSION)
    }

    List<RegexFilter> getRegexFilters(String platform = "PC") {
        sql.rows(GET_FILTERS, [platform]).collect { new RegexFilter(chatId: it.CHATID, expression: it.EXPRESSION) }
    }

    List<String> getChatIds(String platform = "PC") {
        sql.rows(GET_CHATS, [platform]).CHATID
    }

    def saveChat(Chat chat) {
        sql.executeUpdate(SAVE_CHAT, [chat.id])
    }

    def savePlatform(Chat chat, Platform platform) {
        sql.executeUpdate(SAVE_PLATFORM, [chat.id, platform.toString()])
    }

    def saveFilter(Chat chat, Pattern expression) {
        sql.executeUpdate(SAVE_FILTER, [chat.id, expression.toString()])
    }

    boolean wasNotified(String chatId, String alertId) {
        sql.firstRow(FIND_NOTIFICATION, [chatId, alertId])
    }

    boolean notify(String chatId, String alertId) {
        sql.executeUpdate(INSERT_NOTIFICATION, [chatId, alertId])
    }

    def saveTimeZone(Chat chat, TimeZone timeZone) {
        sql.executeUpdate(SAVE_TIMEZONE, [chat.id, timeZone.ID])
    }

    Platform getPlatform(String chatId) {
        Platform.valueOf sql.firstRow(FIND_PLATFORM, [chatId]).PLATFORM.toString()
    }

    TimeZone getTimeZone(String chatId) {
        TimeZone.getTimeZone sql.firstRow(FIND_TIMEZONE, [chatId]).TIMEZONEID.toString()
    }

}
