package com.jj.cephalonvigil.model

/**
 * Created by D053411 on 11/6/2016.
 */
class MissionInfo {
    String missionType
    String faction
    String location
    String levelOverride
    String enemySpec
    int minEnemyLevel
    int maxEnemyLevel
    double difficulty
    int seed
    int maxWaveNum
    boolean archwingRequired

    MissionReward missionReward

    String getPrettyString() {
        [I18N.get(location), I18N.get(faction), I18N.get(missionType) + (archwingRequired ? " (Archwing)" : ""),
                "$minEnemyLevel-$maxEnemyLevel"].join(" | ") + "\n" +
                missionReward.prettyReward
    }

    @Override
    public String toString() {
        return "MissionInfo{" +
                "missionType='" + missionType + '\'' +
                ", faction='" + faction + '\'' +
                ", location='" + location + '\'' +
                ", levelOverride='" + levelOverride + '\'' +
                ", enemySpec='" + enemySpec + '\'' +
                ", minEnemyLevel=" + minEnemyLevel +
                ", maxEnemyLevel=" + maxEnemyLevel +
                ", difficulty=" + difficulty +
                ", seed=" + seed +
                ", maxWaveNum=" + maxWaveNum +
                ", missionReward=" + missionReward +
                '}';
    }
}
