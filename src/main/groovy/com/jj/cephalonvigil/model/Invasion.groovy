package com.jj.cephalonvigil.model

/**
 * Created by D053411 on 11/8/2016.
 */
class Invasion {

    private static final String FC_INFESTATION = "FC_INFESTATION"

    ObjectId _id
    String Faction
    String Node
    int Count
    int Goal
    String LocTag
    boolean Completed

    MissionReward AttackerReward
    InvasionMissionInfo AttackerMissionInfo

    MissionReward DefenderReward
    InvasionMissionInfo DefenderMissionInfo

    Activation Activation

    String getId() {
        "i_" + _id.$oid
    }

    boolean getHasAttackerMission() {
        Faction != FC_INFESTATION
    }

    String getPrettyDefenderRewards() {
        [DefenderMissionInfo.missionReward?.creditReward, DefenderReward.itemRewards].findAll().join(", ")
    }

    String getPrettyAttackerRewards() {
        [AttackerMissionInfo.missionReward?.creditReward, AttackerReward.itemRewards].findAll().join(", ")
    }

    String getPrettyMissionInfo() {
        [I18N.get(Node), I18N.get(LocTag)].join " | "
    }

    String getPrettyString() {
            prettyMissionInfo + "\n" + (hasAttackerMission ? (
             [prettyDefenderRewards, prettyAttackerRewards].join(" vs. ")
            ) : prettyDefenderRewards)

    }

    @Override
    public String toString() {
        return "Invasion{" +
                "_id=" + _id +
                ", Faction='" + Faction + '\'' +
                ", Node='" + Node + '\'' +
                ", Count=" + Count +
                ", Goal=" + Goal +
                ", LocTag='" + LocTag + '\'' +
                ", Completed=" + Completed +
                ", AttackerReward=" + AttackerReward +
                ", AttackerMissionInfo=" + AttackerMissionInfo +
                ", DefenderReward=" + DefenderReward +
                ", DefenderMissionInfo=" + DefenderMissionInfo +
                ", Activation=" + Activation +
                '}';
    }
}
