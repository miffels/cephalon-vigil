package com.jj.cephalonvigil.model

/**
 * Created by D053411 on 11/6/2016.
 */
class ObjectId {
    String $oid

    @Override
    public String toString() {
        return "ObjectId{" +
                "\$id='" + $id + '\'' +
                '}';
    }
}
