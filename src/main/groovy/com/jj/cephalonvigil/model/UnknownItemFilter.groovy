package com.jj.cephalonvigil.model
/**
 * Created by Michael on 8/11/2016.
 */
class UnknownItemFilter extends Filter {

    /**
     * Chat ID of the bot owner
     */
    String OWNER_CHAT_ID = "240386757"

    @Override
    boolean filterItem(String itemId) {
        I18N.get(itemId) == itemId
    }

    @Override
    String getChatId() {
        return OWNER_CHAT_ID
    }
}
