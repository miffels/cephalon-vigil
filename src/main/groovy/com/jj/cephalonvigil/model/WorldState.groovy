package com.jj.cephalonvigil.model

import com.fasterxml.jackson.annotation.JsonIgnoreProperties

/**
 * Created by D053411 on 11/6/2016.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
class WorldState {

    List<Alert> Alerts
    List<Invasion> Invasions
    List<PersistentEnemy> PersistentEnemies

    @Override
    public String toString() {
        return "WorldState{" +
                "Alerts=" + Alerts +
                ",Invasions=" + Invasions +
                ",PersistentEnemies=" + PersistentEnemies +
                '}';
    }
}
