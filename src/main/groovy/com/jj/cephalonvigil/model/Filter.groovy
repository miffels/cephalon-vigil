package com.jj.cephalonvigil.model
/**
 * Created by Michael on 8/11/2016.
 */
abstract class Filter {

    List<Alert> alerts(List<Alert> alerts) {
        alerts.findAll {
            it.MissionInfo.missionReward.items?.find {
                filterItem it
            } || it.MissionInfo.missionReward.countedItems?.find {
                filterItem it.ItemType
            }
        }
    }

    List<Invasion> invasions(List<Invasion> invasions) {
        invasions.findAll {
            !it.Completed
        }.findAll {
            List<MissionReward> rewards = [it.AttackerReward, it.DefenderReward]
            rewards*.items.flatten().findAll().find {
                filterItem it
            } || rewards*.countedItems.flatten().findAll().find {
                filterItem it.ItemType
            }
        }
    }

    abstract boolean filterItem(String itemId)
    abstract String getChatId()

}
