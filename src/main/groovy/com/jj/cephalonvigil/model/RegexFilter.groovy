package com.jj.cephalonvigil.model

import java.util.regex.Pattern

/**
 * Created by Michael on 8/11/2016.
 */
class RegexFilter extends Filter {

    String chatId
    Pattern pattern

    @Override
    boolean filterItem(String itemId) {
        pattern.matcher(itemId).find() || pattern.matcher(I18N.get(itemId)).find()
    }

    void setExpression(String expression) {
        pattern = Pattern.compile expression
    }
}
