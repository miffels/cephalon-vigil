package com.jj.cephalonvigil.model

/**
 * Created by d053411 on 2/12/2017.
 */
class PersistentEnemy {
    ObjectId _id
    String AgentType
    boolean Discovered
    String LastDiscoveredLocation
    MongoDate LastDiscoveredTime

    String getEventId() {
        (Discovered ? "a1_" : "a0_") + (LastDiscoveredTime as Date).time
    }

    String getPrettyAgentName() {
        I18N.get AgentType
    }

    String getPrettyLocation() {
        I18N.get LastDiscoveredLocation
    }
}
