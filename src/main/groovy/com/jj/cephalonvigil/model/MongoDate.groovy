package com.jj.cephalonvigil.model

/**
 * Created by d053411 on 2/12/2017.
 */
class MongoDate {

    static class MongoDateInner {
        Long $numberLong
    }

    MongoDateInner $date

    public asType(Class c) {
        if (c == Date.class) new Date($date.$numberLong)
    }
}
