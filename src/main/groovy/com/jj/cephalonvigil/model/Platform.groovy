package com.jj.cephalonvigil.model

/**
 * Created by D053411 on 11/15/2016.
 */
enum Platform {
    PC,
    PS4,
    XB1
}
