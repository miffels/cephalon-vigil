package com.jj.cephalonvigil.model

/**
 * Created by d053411 on 11/6/2016.
 */
class I18N {

    static final ResourceBundle MESSAGES = ResourceBundle.getBundle("i18n/messages", Locale.ENGLISH);

    static String get(String key) {
        MESSAGES.keySet().contains(key) ? MESSAGES.getString(key) : key
    }

}
