package com.jj.cephalonvigil.model

/**
 * Created by Michael on 8/11/2016.
 */
class Alert {

    ObjectId _id
    MongoDate Activation
    MongoDate Expiry
    MissionInfo MissionInfo

    String getId() {
        "a_" + _id.$oid
    }

    @Override
    public String toString() {
        return "Alert{" +
                "_id=" + _id +
                ", Activation=" + Activation +
                ", Expiry=" + Expiry +
                ", MissionInfo=" + MissionInfo +
                '}';
    }
}
