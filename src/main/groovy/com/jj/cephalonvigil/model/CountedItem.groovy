package com.jj.cephalonvigil.model

/**
 * Created by D053411 on 11/6/2016.
 */
class CountedItem {
    String ItemType
    int ItemCount

    @Override
    public String toString() {
        return "CountedItem{" +
                "ItemType='" + ItemType + '\'' +
                ", ItemCount=" + ItemCount +
                '}';
    }
}
