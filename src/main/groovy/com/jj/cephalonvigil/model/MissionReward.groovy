package com.jj.cephalonvigil.model

/**
 * Created by D053411 on 11/6/2016.
 */
class MissionReward {

    int credits
    List<String> items
    List<CountedItem> countedItems

    String getCreditReward() {
        credits ? credits + "Cr" : null
    }

    String getItemRewards() {
        (items?.collect { I18N.get(it) } ?:
                countedItems?.collect { "$it.ItemCount ${I18N.get(it.ItemType)}" })?.join(", ")
    }

    String getPrettyReward() {
        creditReward ?
                (itemRewards ? [creditReward, itemRewards].join(", ") : creditReward) :
                itemRewards
    }

    @Override
    public String toString() {
        return "MissionReward{" +
                "credits=" + credits +
                (items ? ", items=" + items : ", countedItems=" + countedItems) +
                '}';
    }
}
