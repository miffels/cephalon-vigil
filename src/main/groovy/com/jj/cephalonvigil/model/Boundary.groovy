package com.jj.cephalonvigil.model

/**
 * Created by D053411 on 11/6/2016.
 */
class Boundary {
    long sec
    int usec

    Date getDate() {
        new Date((sec*1000l + usec/1e3).longValue())
    }

    @Override
    public String toString() {
        return "Boundary{" + date + "}";
    }
}

class Activation extends Boundary {}
class Expiry extends Boundary {}
