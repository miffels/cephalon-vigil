package com.jj.cephalonvigil.model

/**
 * Created by D053411 on 11/6/2016.
 */
class InvasionMissionInfo extends MissionInfo {

    @Override
    String getPrettyString() {
        [I18N.get(location), "$minEnemyLevel-$maxEnemyLevel"].join(" | ")
    }

    @Override
    public String toString() {
        return "InvasionMissionInfo{" +
                "missionType='" + missionType + '\'' +
                ", faction='" + faction + '\'' +
                ", location='" + location + '\'' +
                ", levelOverride='" + levelOverride + '\'' +
                ", enemySpec='" + enemySpec + '\'' +
                ", minEnemyLevel=" + minEnemyLevel +
                ", maxEnemyLevel=" + maxEnemyLevel +
                ", difficulty=" + difficulty +
                ", seed=" + seed +
                ", maxWaveNum=" + maxWaveNum +
                ", missionReward=" + missionReward +
                '}';
    }
}
